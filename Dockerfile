FROM node:21.6.2-alpine3.19

# Create app directory
WORKDIR /usr/src/app

COPY yarn.lock package.json ./

RUN yarn install

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]